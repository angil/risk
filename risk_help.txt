RRRRRRRRRRRRRRRRR   IIIIIIIIII   SSSSSSSSSSSSSSS KKKKKKKKK    KKKKKKK
R::::::::::::::::R  I::::::::I SS:::::::::::::::SK:::::::K    K:::::K
R::::::RRRRRR:::::R I::::::::IS:::::SSSSSS::::::SK:::::::K    K:::::K
RR:::::R     R:::::RII::::::IIS:::::S     SSSSSSSK:::::::K   K::::::K
  R::::R     R:::::R  I::::I  S:::::S            KK::::::K  K:::::KKK
  R::::R     R:::::R  I::::I  S:::::S              K:::::K K:::::K   
  R::::RRRRRR:::::R   I::::I   S::::SSSS           K::::::K:::::K    
  R:::::::::::::RR    I::::I    SS::::::SSSSS      K:::::::::::K     
  R::::RRRRRR:::::R   I::::I      SSS::::::::SS    K:::::::::::K     
  R::::R     R:::::R  I::::I         SSSSSS::::S   K::::::K:::::K    
  R::::R     R:::::R  I::::I              S:::::S  K:::::K K:::::K   
  R::::R     R:::::R  I::::I              S:::::SKK::::::K  K:::::KKK
RR:::::R     R:::::RII::::::IISSSSSSS     S:::::SK:::::::K   K::::::K
R::::::R     R:::::RI::::::::IS::::::SSSSSS:::::SK:::::::K    K:::::K
R::::::R     R:::::RI::::::::IS:::::::::::::::SS K:::::::K    K:::::K
RRRRRRRR     RRRRRRRIIIIIIIIII SSSSSSSSSSSSSSS   KKKKKKKKK    KKKKKKK

		   The Game of Virtual Domination
		By: Jamie Langille and Christopher Ng
	        	   January 2009

------------------------------------------

NEW GAME AND GAME INITIALIZATION

To make a game, go to the game menu option and select <New Game> (ctrl+N) 
You will then be able to choose that number of human players you want in the game, change the names of the players, and select each player�s own colours. If you check the random countries box, each player will be assigned a random country on the map. Otherwise each player will need to choose countries on their own, by placing one Trojan.
  
Once every country is owned by a player, players will then add more Trojans onto countries as they please until they run out of reinforcements. Game play will start with the first player.

------------------------------------------

UNITS

Trojan � 1 unit
Virus � 5 units
Worm � 10 units 

------------------------------------------

TURNs

Each turn will begin with the user placing more reinforcements, except for the first player�s first turn.  The reinforcements they get are determined by the number of countries they own divided by three, rounded down. If a player owns a continent, they get the bonus belonging to the continent added to their other reinforcements.  The player could then select one of their own countries to attack another neighboring country. Once the attacks are completed, the player then has the option to move units from one country to another bordering country. If the player successfully conquered a country, the player gets a risk card.  

------------------------------------------

RISK CARDS

Risk cards each have a unit value and a country. There are also two wild cards; each card has all three different types of unit on them, but no country.  If a player has three units of the same kind, or three different units, the player will trade the three card combo in for extra units at the beginning of their turn. If a player owns a country on the card, the player upon trading in the combo will get a bonus amount of units on that country.  Risk cards owned by a player are indicated in the game by small rectangles in the player�s user panel.  


------------------------------------------

USER PANEL

The user panel is at the bottom of the screen. On the far left side, the empty box will display information about countries when moused over. To the right are player squares, where each player�s name appears in a separate square, and information about the player, with their colour.  There is also a small red triangle above the current player�s turn.  

------------------------------------------

ATTACK WINDOW

Once the attack window is opened, the attacking player appears on the right, defender on the left. Each player can choose the number of dice they want to attack with.  The maximum number of dice a player can use is three (for the attacker) and two (for the defender) unless the player does not have enough units to attack with.  A player will need at least one unit on every owning country as defense, any extras can be used for attacking, and get one die for every attacking Trojan.  Once both players are ready to fight, they press attack. The highest roll of each player is compared with the other.  If one is higher, the owner of the lower die loses one Trojan, if two dice were rolled, two Trojans will be deleted from the owner.  The defender wins ties.  Upon conquering a country, the winning country will move to the conqered country a minimum of the same number of dice of Trojans, to the conquered country. 

------------------------------------------

CONTROLS

Most of the in game controls are dependant on the player using the mouse to select countries and the number of dice to use. To end your turn, press <ESC>. The game is most fun when every player has their own mouse to use.

When placing armies, you may left-click to place one, right-click to place five, and centre-click to place ten armies.

------------------------------------------

For more information about how to play risk (origional version
Go to http://www.hasbro.com/common/instruct/Risk1980.PDF


How does this game differ from my board game?
The only difference is the map, as well as a small change with the Risk Packs. Instead of choosing one of the countries to place two extra armies on, you place an extra two on EVERY ONE YOU OWN. This makes Risk Packs more potent, as well as easier to make.

------------------------------------------

Commonly Asked Questions:

Q:	How do I end my turn?
A:	To end your turn, exit a popup window, or end the fortifying phase, press 'ESC'.

Q:	My game lags. Is there something wrong with it?
A:	Probably not. This game was made with Java, which is naturally a slower language. If the game is abnormally slow (more then a minute), please E-Mail jamiediver42@hotmail.com (Jamie) with the title "Risk Glitch" to tell us about it. One error that is known is when you go to a new game and don't change any colors - you NEED to change at least one color for the game to be operational.