@echo off
title Risk Starter
color 1a
echo RRRRRRRRRRRRRRRRR   IIIIIIIIII   SSSSSSSSSSSSSSS KKKKKKKKK    KKKKKKK
echo R::::::::::::::::R  I::::::::I SS:::::::::::::::SK:::::::K    K:::::K
echo R::::::RRRRRR:::::R I::::::::IS:::::SSSSSS::::::SK:::::::K    K:::::K
echo RR:::::R     R:::::RII::::::IIS:::::S     SSSSSSSK:::::::K   K::::::K
echo   R::::R     R:::::R  I::::I  S:::::S            KK::::::K  K:::::KKK
echo   R::::R     R:::::R  I::::I  S:::::S              K:::::K K:::::K   
echo   R::::RRRRRR:::::R   I::::I   S::::SSSS           K::::::K:::::K    
echo   R:::::::::::::RR    I::::I    SS::::::SSSSS      K:::::::::::K     
echo   R::::RRRRRR:::::R   I::::I      SSS::::::::SS    K:::::::::::K     
echo   R::::R     R:::::R  I::::I         SSSSSS::::S   K::::::K:::::K    
echo   R::::R     R:::::R  I::::I              S:::::S  K:::::K K:::::K   
echo   R::::R     R:::::R  I::::I              S:::::SKK::::::K  K:::::KKK
echo RR:::::R     R:::::RII::::::IISSSSSSS     S:::::SK:::::::K   K::::::K
echo R::::::R     R:::::RI::::::::IS::::::SSSSSS:::::SK:::::::K    K:::::K
echo R::::::R     R:::::RI::::::::IS:::::::::::::::SS K:::::::K    K:::::K
echo RRRRRRRR     RRRRRRRIIIIIIIIII SSSSSSSSSSSSSSS   KKKKKKKKK    KKKKKKK
echo ---------------------------------------------------------------------
echo 		        The Game of Virtual Domination
echo 		    By: Jamie Langille and Christopher Ng
echo         		        January 2009
echo Playing Risk...
java Risk
echo Done.