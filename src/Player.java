import java.awt.Color;
import java.util.ArrayList;

/**
 * Constructs a basic player object
 *
 * @author Jamie Langille
 * @version NOT COMPLETED
 */

public class Player
{
	private Color color;
	private String name;
	public int armiesToPlace;
	private ArrayList < RiskCard > cards;
	boolean isCPU;

	public Player (String name, Color color)
	{
		this.name = name;
		this.color = color;
		armiesToPlace = 0;
		cards = new ArrayList < RiskCard > (0);
	}


	public Player ()
	{
		name = "CPU";
		color = new Color ((int) (Math.random () * 255 + 1), (int) (Math.random () * 255 + 1), (int) (Math.random () * 255 + 1));
	}


	public void addCard (RiskCard card)
	{
		cards.add (card);
	}


	public RiskCard[] getCards ()
	{
		RiskCard[] cardList = new RiskCard [cards.size ()];
		for (int card = 0 ; card < cards.size () ; card++)
		{
			cardList [card] = cards.get (card);
		}
		return cardList;
	}


	public RiskCard[] getPack ()
	{
		if (cards.size () >= 3)
		{
			for (int card = 0 ; card < cards.size () - 2 ; card++)
			{
				for (int secondCard = card + 1 ; secondCard < cards.size () - 1 ; secondCard++)
				{
					for (int thirdCard = secondCard + 1 ; thirdCard < cards.size () ; thirdCard++)
					{
						if ((cards.get(card).getValue() == -1 || cards.get (secondCard).getValue () == -1 || cards.get (thirdCard).getValue () == -1) ||
								(cards.get (card).getValue () == cards.get (secondCard).getValue () && cards.get (secondCard).getValue () == cards.get (thirdCard).getValue ()) || (cards.get (card).getValue () != cards.get (secondCard).getValue () && cards.get (secondCard).getValue () != cards.get (thirdCard).getValue () && cards.get (thirdCard).getValue () != cards.get (card).getValue ()))
						{
							RiskCard[] pack =  
							{
									cards.get (card), cards.get (secondCard), cards.get (thirdCard)
							};
							if (cards.contains(pack[0]))
								cards.remove(pack[0]);
							if (cards.contains(pack[1]))
								cards.remove(pack[1]);
							if (cards.contains(pack[2]))
								cards.remove(pack[2]);
							return pack;
						}
					}
				}
			}
		}
		return null;
	}


	public Color getColor ()
	{
		return color;
	}


	public void changeName (String name)
	{
		this.name = name;
	}


	public String getName ()
	{
		return name;
	}


	public String toString ()
	{
		return "";
	}


	public void setName (String readLine)
	{
		name = readLine;
	}


	public void setColor (Color color2)
	{
		color = color2;
	}


	public int getNoOfCards() {
		return cards.size();
	}
}
