
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Plays a game of <b>Risk</b>, with an internet theme
 * @author Jamie Langille and Christopher Ng
 * @version Jan. 2009
 */

@SuppressWarnings("serial")
public class Risk extends JFrame implements ActionListener{
	private BoardPanel boardPanel;
	private UserPanel userPanel;
	public static Country[] countries;
	private Continent[] continents;
	private Player[] players;
	private RiskDeck deck;
	private final int[] packBonus = {4, 6, 8, 10};
		//{4, 6, 8, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100};
	private int currentBonus;
	private JMenuItem exitOption, newOption, musicOption, saveOption, openOption, colorOption, nameOption, aboutOption, unsureOption, readMeOption;
	private int phase;
	private int currentPlayer;
	private boolean attacking;
	private boolean successfulAttack;

	boolean music = false;
	AudioClip song;

	/**
	 * Creates the <b>Risk</b> Window and game.
	 * @throws IOException	if <code><b>Risk</b> Map Points.txt</code> does not exist.
	 */
	public Risk() throws IOException{

		// set window attributes
		setLocation (0, 0);
		setTitle ("Risk");
		setResizable (false);

		// Container & Panes
		Container contentPane = getContentPane ();
		Dimension size = new Dimension (800, 600);

		// Sets up BoardPanel and UserPanel (inner classes - extend JPanel)
		boardPanel = new BoardPanel (size);
		size = new Dimension (700, 100);
		userPanel = new UserPanel (size);
		contentPane.add (boardPanel, BorderLayout.NORTH);
		contentPane.add(userPanel, BorderLayout.SOUTH);

		// menu items
		exitOption = new JMenuItem ("Exit");
		exitOption.addActionListener(this);

		newOption = new JMenuItem ("New Game");
		newOption.addActionListener(this);
		newOption.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));

		musicOption = new JMenuItem ("Music On/Off");
		musicOption.addActionListener (this);

		saveOption = new JMenuItem("Save");
		saveOption.addActionListener(this);
		saveOption.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));

		openOption = new JMenuItem("Open");
		openOption.addActionListener(this);
		openOption.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));

		colorOption = new JMenuItem("Color");
		colorOption.addActionListener(this);

		nameOption = new JMenuItem("Name");
		nameOption.addActionListener(this);

		aboutOption = new JMenuItem("About");
		aboutOption.addActionListener(this);

		unsureOption = new JMenuItem("What do I do now?");
		unsureOption.addActionListener(this);

		readMeOption = new JMenuItem("Read Me");
		readMeOption.addActionListener(this);

		// add items to menu bar
		JMenu gameMenu = new JMenu ("Game");
		gameMenu.add(newOption);
		gameMenu.add(saveOption);
		gameMenu.add(openOption);
		gameMenu.add(exitOption);

		JMenu editMenu = new JMenu("Edit");
		editMenu.add(colorOption);
		editMenu.add(nameOption);

		JMenu helpMenu = new JMenu("Help");
		helpMenu.add(unsureOption);
		helpMenu.add(readMeOption);
		helpMenu.add(aboutOption);

		JMenu optionsMenu = new JMenu("Options");
		optionsMenu.add(musicOption);

		JMenuBar menuBar = new JMenuBar();
		menuBar.add(gameMenu);
		menuBar.add(editMenu);
		menuBar.add(optionsMenu);
		menuBar.add(helpMenu);

		setJMenuBar (menuBar);

		// loads countries for use in a game
		loadPlaces();
	}

	/**
	 * Reloads countries and continents
	 * @throws IOException	if <code><b>Risk</b> Map Points.txt</code> does not exist.
	 */
	public void loadPlaces() throws IOException{

		// refresh countries and continents
		countries = new Country[42];
		continents = new Continent[6];

		// read in from file - keep track of current country/continent as I read
		BufferedReader in = new BufferedReader (new FileReader("Risk Map Points.txt"));
		int currentCountry = -1;
		int currentContinent = -1;

		// read until blank line (seperation between country points/names
		// and connections)
		String input = in.readLine();
		while (!input.equals("")){

			// reads in continent bonus, name, number of countries,
			// and each countries' information 
			continents[++currentContinent] = new Continent(Integer.parseInt(input));
			continents[currentContinent].setName(in.readLine());
			input = in.readLine();
			int noOfCountries = Integer.parseInt(input);
			for (int country = 0; country < noOfCountries; country++){
				countries[++currentCountry] = new Country(in.readLine());
				// name
				StringTokenizer pts = new StringTokenizer(in.readLine(), ",;", false);
				while (pts.hasMoreTokens()){
					countries[currentCountry].addPoint(Integer.parseInt(pts.nextToken()), Integer.parseInt(pts.nextToken()));
				}
				continents[currentContinent].addCountry(countries[currentCountry]);
			}

			input = in.readLine();
		}

		// skip over blank space
		input = in.readLine();

		// read in how countries connect to each other
		for (int country = 0; country < countries.length; country++){
			StringTokenizer conn = new StringTokenizer(input, ",;", false);

			while (conn.hasMoreTokens()){
				countries[country].addConnection(countries[Integer.parseInt(conn.nextToken()) - 1]);
			}

			input = in.readLine();
		}

		in.close();

		// initialize the risk deck (of cards) and the current bonus value
		deck = new RiskDeck();
		currentBonus = -1;
	}

	/**
	 * Loads a new game giving players random countries or not.
	 * @param randomCountries	if countries will be randomly selected
	 * @throws IOException		if <code><b>Risk</b> Map Points.txt</code> cannot be found.
	 */
	public void newGame (boolean randomCountries) throws IOException{

		// need to reset the board
		loadPlaces();

		// loads random countries & sets the phase so that
		// players do not select thier own.
		if (randomCountries) {

			// cycling players, set a random country
			//(that hasn't been selected yet) that player.
			boolean[] selected = new boolean[countries.length];
			int player = 1;
			for (int rep = 0; rep < 42; rep++){
				int rand = (int)(Math.random() * countries.length);
				while (selected[rand]){
					rand = (int)(Math.random() * countries.length);
				}
				countries[rand].setOwningPlayer(player);
				countries[rand].addArmies(1);
				players[player].armiesToPlace--;
				player++;
				if (player == players.length)
					player = 1;
				selected[rand] = true;
			}
			phase = 1;

			// show message telling users to place extra armies
			JOptionPane.showMessageDialog(boardPanel, "Place your extra armies.", "MESSAGE", JOptionPane.INFORMATION_MESSAGE);
		}

		// select your own
		else {
			phase = 0;
			JOptionPane.showMessageDialog(boardPanel, "Select your countries.", "MESSAGE", JOptionPane.INFORMATION_MESSAGE);
		}

		// sets the current player
		currentPlayer = 1;
	}

	/**
	 * Retrieves the total ammount of armies the current player
	 * recieves at any point in time. Displays messages if they
	 * get extra bonuses (continents, Risk Packs, etc.)
	 * @return 		the number of armies that the current player recieves
	 */
	public int getArmies(boolean packOnly){

		// count countries owned by player
		int ammount = 0;
		if (!packOnly){
			for (Country country : countries){
				if (country.isOwnedBy(currentPlayer)){
					ammount++;
				}
			}

			// divide by 3 (integer division - like in the game)
			// e.g., you would get 4 armies for owning 14 countries.
			ammount /= 3;

			// check is player gets any continent bonuses & display
			// message if they do
			for (Continent continent : continents){
				if (continent.getBonus(currentPlayer) != 0){
					JOptionPane.showMessageDialog(boardPanel, "You own " + continent.getName() + " " + players[currentPlayer].getName() + "!\nYou get " + continent.getBonus(currentPlayer) + " more armies to place!", "MESSAGE", JOptionPane.INFORMATION_MESSAGE);
				}
				ammount += continent.getBonus(currentPlayer);
			}
		}
		// check if player has a risk pack to cash in,
		// and change the current bonus accordingly
		RiskCard[] cards = players[currentPlayer].getPack();
		while (cards != null){
			if (currentBonus < packBonus.length){
				ammount += packBonus[++currentBonus];
				JOptionPane.showMessageDialog(boardPanel, "You got a Risk Pack " + players[currentPlayer].getName() + "!\nYou get " + packBonus[currentBonus] + " more armies to place!", "Risk Pack", JOptionPane.INFORMATION_MESSAGE);
			}
			else if (currentBonus == packBonus.length){
				currentBonus = packBonus[packBonus.length - 1] + 5;
				ammount += currentBonus;
				JOptionPane.showMessageDialog(boardPanel, "You got a Risk Pack " + players[currentPlayer].getName() + "!\nYou get " + currentBonus + " more armies to place!", "Risk Pack", JOptionPane.INFORMATION_MESSAGE);
			}
			else{
				currentBonus += 5;
				ammount += currentBonus;
				JOptionPane.showMessageDialog(boardPanel, "You got a Risk Pack " + players[currentPlayer].getName() + "!\nYou get " + currentBonus + " more armies to place!", "Risk Pack", JOptionPane.INFORMATION_MESSAGE);
			}
			for (RiskCard card : cards){
				if (card.getCountry().getOwningPlayer() == currentPlayer){
					JOptionPane.showMessageDialog(boardPanel, "Plus two armies to " + card.getCountry().getName(), "Extra Armies to" + card.getCountry().getName(), JOptionPane.INFORMATION_MESSAGE);
					card.getCountry().addArmies(2);
				}
			}	
			cards = players[currentPlayer].getPack();
		}

		// return the final ammount, with a minimum of three armies
		if (ammount >= 3)
			return ammount;
		return 3;
	}

	/**
	 * Gets a .<b>risk</b> file for opening/saving games
	 * @return 		the path of the file to open/write to
	 */
	private String getFile ()
	{
		// make a FileChooser
		JFileChooser fc = new JFileChooser ();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Risk File (*.risk)", "risk"); // != work on mac, so just commented out while working on mac
		fc.setFileFilter(filter);
		fc.setCurrentDirectory(new File ("M:/Eclipse/Risk/"));
		int result = fc.showOpenDialog (null);
		File file = null;

		// return a result that ends with .risk extention
		if (result == JFileChooser.APPROVE_OPTION)
		{
			file = fc.getSelectedFile ();
			if (file.getName().matches(".*\\.risk"))
				return file.getPath ();
			else
				return file.getPath() + ".risk";
		}
		return null;
	}

	/**
	 * Listens for any ActionEvents that occur in <b>Risk</b>.
	 * Such items would include menu items, etc.
	 * Implemented method for java.awt.event.ActionListener.actionPerformed
	 */
	public void actionPerformed(ActionEvent event) {
		if (event.getSource () == exitOption){
			System.exit(0);
		}
		else if (event.getSource() == newOption){
			// makes a new game by calling a NewGameWindow to pop up.
			NewGameWindow newWindow = new NewGameWindow();
			newWindow.setVisible(true);
		}
		else if (event.getSource() == colorOption){
			// allows the current player to change thier color mid-game
			if (players != null){
				Color newColor = JColorChooser.showDialog(null, "Choose a Color, " + players[currentPlayer].getName(), players[currentPlayer].getColor());
				if (newColor != null)
					players[currentPlayer].setColor(newColor);
			}
		}
		else if (event.getSource() == nameOption){
			// allows the current player to change thier name mid-game
			if (players != null){
				String newName = JOptionPane.showInputDialog(this, "Change Your Name, " + players[currentPlayer].getName(), players[currentPlayer].getName());
				if (newName != null)
					players[currentPlayer].changeName(newName);
			}
		}
		else if (event.getSource() == aboutOption){
			JOptionPane.showMessageDialog(boardPanel, "RISK\nMade by: Jamie Langille\nand Christopher Ng\nJan. 2009", "About Risk", JOptionPane.INFORMATION_MESSAGE, null);
		}
		else if (event.getSource() == readMeOption){
			try {
				Runtime.getRuntime().exec("cmd /c start risk_help.txt");
			} catch (IOException e) {
				JOptionPane.showMessageDialog(boardPanel, "Could not find \"risk_help.txt\"", "ReadME", JOptionPane.INFORMATION_MESSAGE, null);
			}
		}
		else if (event.getSource() == unsureOption){
			if (players != null){
				String task = "";
				switch (phase){
				case 0:
					task = "selecting a country"; break;
				case 1:
					task = "placing an army"; break;
				case 2:
					task = "placing armies"; break;
				case 3:
					task = "attacking"; break;
				case 4:
					task = "fortifying"; break;
				default:
					task = "not doing anything";
				}
				JOptionPane.showMessageDialog(boardPanel, "Right now, " + players[currentPlayer].getName() + " is " + task + ".", "What do I do now?", JOptionPane.INFORMATION_MESSAGE, null);
			}
			else{
				JOptionPane.showMessageDialog(boardPanel, "Start a game.\nGame->New Game", "What do I do now?", JOptionPane.INFORMATION_MESSAGE, null);
			}
		}
		else if (event.getSource() == musicOption)
		{
			// plays/stops music (default off)
			if (!music)
			{
				URL backgroundURL = Risk.class.getResource ("game11.mid");
				if (backgroundURL != null)
				{
					song = Applet.newAudioClip(backgroundURL);
					song.loop();
					music = true;
				}
			}
			else
			{
				song.stop();
				music = false;
			}
		}
		else if (event.getSource() == saveOption){
			// saves a game to a .risk file
			try {
				// get the file
				PrintWriter out = new PrintWriter(getFile());

				// print global data
				out.println(phase);
				out.println(currentPlayer);
				out.println(players.length);
				out.println(currentBonus);
				
				// print player stats
				for (Player player : players){
					out.println(player.getName());
					out.println(player.getColor().getRed());
					out.println(player.getColor().getGreen());
					out.println(player.getColor().getBlue());
					out.println(player.armiesToPlace);

					// including RiskCard stats owned by that player
					RiskCard[] cards = player.getCards();
					out.println(cards.length);
					for (RiskCard card : cards){
						for (int i = 0; i < countries.length; i++){
							if(card.getCountry() == countries[i]){
								out.println(card.getValue() + " " + i);
								break;
							}
						}
					}
				}

				// print country stats (index & owning player)
				for (Country country : countries){
					out.println(country.getArmies() + " " + country.getOwningPlayer());
				}

				out.close();
			} catch (Exception e){}
		}
		else if (event.getSource() == openOption){
			// opens a game from a .risk file
			try {

				// refresh the board
				loadPlaces();

				// get the file
				BufferedReader in = new BufferedReader (new FileReader(getFile()));

				// read global data
				phase = Integer.parseInt(in.readLine());
				currentPlayer = Integer.parseInt(in.readLine());
				players = new Player[Integer.parseInt(in.readLine())];
				currentBonus = Integer.parseInt(in.readLine());

				// read player stats
				for (int player = 0; player < players.length; player++){
					players[player] = new Player(in.readLine(), new Color(Integer.parseInt(in.readLine()), Integer.parseInt(in.readLine()), Integer.parseInt(in.readLine())));
					players[player].armiesToPlace = Integer.parseInt(in.readLine());

					// and cards
					RiskCard[] cards = new RiskCard [Integer.parseInt(in.readLine())];
					for (int card = 0; card < cards.length; card++){
						String input = in.readLine();
						cards[card] = new RiskCard(countries[Integer.parseInt(input.substring(input.indexOf(' ') + 1))], Integer.parseInt(input.substring(0, input.indexOf(' '))));
						players[player].addCard(cards[card]);
					}
				}

				// read country stats
				for (int country = 0; country < countries.length; country++){
					String input = in.readLine();
					countries[country].setOwningPlayer(Integer.parseInt(input.substring(input.indexOf(' ') + 1)));
					countries[country].removeArmies(countries[country].getArmies());
					countries[country].addArmies(Integer.parseInt(input.substring(0, input.indexOf(' '))));
				} 

				in.close();

			} catch (Exception e) {}
		}
	}

	/**
	 * Creates an window to handle attack events with GUI and pictures
	 * @author Jamie Langille and Christopher Ng
	 * @version Jan. 2009
	 */
	private class AttackWindow extends JFrame implements ActionListener, ItemListener, ChangeListener, WindowListener
	{
		// Variables local to attacking
		private JComboBox attackDiceSelector, defendDiceSelector;
		private JPanel inputPanel;
		private ActionPanel actionPanel;
		private JButton okButton, attackButton;
		private JSlider armySlider;
		private JLabel armies;
		private Country attackCountry, defendCountry;
		private Image attackPic, defendPic;
		private int[] defendDice, attackDice;

		/**
		 * Constructs a new window for one country to attack another
		 * @param attackCountry		the attacking Country Object
		 * @param defendCountry		the defending Country Object
		 */
		public AttackWindow(Country attackCountry, Country defendCountry)
		{
			this.attackCountry = attackCountry;
			this.defendCountry = defendCountry;

			// set window attributes
			setLocation (300, 300);
			setTitle ("Attack!");
			setSize(400, 397);
			repaint();

			// initializes GUI

			// number of dice to attack with
			attackDiceSelector = new JComboBox ();
			for (int no = attackCountry.getNoOfDice(); no > 0; no--){
				attackDiceSelector.addItem(no);
			}
			attackDiceSelector.addItemListener(this);

			// number of dice to defend with
			defendDiceSelector = new JComboBox();
			for (int no = Math.min(defendCountry.getArmies(), 2); no > 0; no--){
				defendDiceSelector.addItem(no);
			}
			defendDiceSelector.addItemListener(this);

			// buttons to quit and attack
			okButton = new JButton("OK");
			okButton.addActionListener(this);
			attackButton = new JButton ("Attack");
			attackButton.addActionListener(this);

			// slider to move armies if defendingCopuntry is defeated
			armySlider = new JSlider(attackCountry.getNoOfDice(), attackCountry.getArmies() - 1, attackCountry.getArmies() - 1);
			armySlider.setVisible(false);
			armySlider.setPaintLabels(true);
			armySlider.setSnapToTicks(true);
			armySlider.addChangeListener(this);

			// JLabel to see how many armies you want to move (if defendingCountry is defeated)
			armies = new JLabel ("            Move: " + armySlider.getValue());
			armies.setVisible(false);

			// sets up panels (for user input & drawing events)
			inputPanel = new JPanel();
			inputPanel.setLayout(new GridLayout(0, 3));
			for (int no = 1; no <= 15; no++){
				inputPanel.add(new JLabel());
			}

			// add GUI components to the inputPanel
			// armySlider and army label are initially invisible,
			// as nothing has happened yet.
			inputPanel.add(defendDiceSelector);
			inputPanel.add(armies);
			inputPanel.add(attackDiceSelector);
			inputPanel.add(okButton);
			inputPanel.add(armySlider);
			inputPanel.add(attackButton);

			// set up the ActionPanel (inner class to handle drawing events)
			actionPanel = new ActionPanel();
			actionPanel.setBounds(0, 0, 400, 260);
			actionPanel.setMaximumSize(new Dimension(400, 310));

			add(actionPanel);
			add(inputPanel);

			this.setResizable(false);
			this.addMouseListener(new MouseHandler());
			this.addWindowListener(this);
		}

		/**
		 * Inner class to handle drawing events for the Attack Window.
		 * @author Jamie Langille and Christopher Ng
		 */
		private class ActionPanel extends JPanel {

			/**
			 * Constructs a new ActionPanel (extends JPanel)
			 */
			public ActionPanel() {
				super();
			}

			/**
			 * Overriding method to javax.swing.JComponent.paintComponent
			 * for drawing events
			 */
			public void paintComponent(Graphics g) {

				// background colors (attack and defend)
				g.setColor(players[defendCountry.getOwningPlayer()].getColor());
				g.fillRect(0, 0, 200, 310);
				g.setColor(players[attackCountry.getOwningPlayer()].getColor());
				g.fillRect(200, 0, 200, 310);

				// loads images (for attack and defend)
				if (attackCountry.getArmies() < 20){
					attackPic = new ImageIcon("oimages\\" + attackCountry.getArmies() + ".png").getImage();	
					if (attackCountry.getArmies() < 4 && attackCountry.getArmies() > 0){
						attackDiceSelector.setSelectedItem(attackCountry.getNoOfDice());
					}
				}
				else if (attackCountry.getArmies() >= 20 && attackCountry.getArmies() < 30){
					attackPic = new ImageIcon("oimages\\20.png").getImage();
				}
				else{
					attackPic = new ImageIcon("oimages\\30.png").getImage();
				}

				if (defendCountry.getArmies() < 20){
					defendPic = new ImageIcon("oimages\\" + defendCountry.getArmies() + ".png").getImage();
					if (defendCountry.getArmies() < 2 && defendCountry.getArmies() > 0){
						defendDiceSelector.setSelectedItem(defendCountry.getArmies());
					}
				}
				else if (defendCountry.getArmies() >= 20 && defendCountry.getArmies() < 30){
					defendPic = new ImageIcon("oimages\\20.png").getImage();
				}
				else{
					defendPic = new ImageIcon("oimages\\30.png").getImage();
				}

				// draws images and how many armies there are
				g.setColor(Color.WHITE);
				g.drawImage(attackPic, 270, 10, null);
				g.drawString("" + attackCountry.getArmies(), 280, 20);
				g.drawImage(defendPic, 20, 10, null);
				g.drawString("" + defendCountry.getArmies(), 30, 20);

				// draws dice (if there are dice to draw)
				if (!(attackDice == null && defendDice == null || attackDice.length == 0 || defendDice.length == 0)){

					for (int die = 0; die < attackDice.length; die++){
						drawDie(210, die * 80 + 10, g, attackDice[die], true);
					}
					for (int die = 0; die < defendDice.length; die++){
						drawDie(140, die * 80 + 10, g, defendDice[die], false);
					}

					// set after-attack settings
					armySlider.setMaximum(attackCountry.getArmies() - 1);

					// if you only have 1 army left, you are done attacking
					if (attackCountry.getArmies() == 1){
						attacking = false;
					}

					// defending country is defeated - change window settings
					else if (defendCountry.getArmies() == 0){
						attackButton.setVisible(false);
						attackDiceSelector.setVisible(false);
						defendDiceSelector.setVisible(false);
						armySlider.setVisible(true);
						armySlider.setMinimum(Integer.parseInt("" + attackDiceSelector.getSelectedItem()));
						armySlider.setMaximum(attackCountry.getArmies() - 1);
						armies.setVisible(true);
					}
				}
			}

			/**
			 * Draws an individual die in a graphics window
			 * @param x			the x co-ordinate of the upper-left corner
			 * @param y			the y co-ordinate of the upper-left corner
			 * @param g			the graphics context to draw in
			 * @param no		the die number (from 1-6)
			 * @param attack	if this die is an attacking die, or a defending one
			 */
			public void drawDie(int x, int y, Graphics g, int no, boolean attack){

				// draws the background
				if (attack)
					g.setColor(Color.RED);
				else
					g.setColor(Color.WHITE);
				g.fillRect(x, y, 50, 50);
				g.setColor(Color.BLACK);
				g.drawRect(x, y, 50, 50);

				// draws die numbers
				if (attack)
					g.setColor(Color.WHITE);
				else
					g.setColor(Color.BLACK);

				// center dot
				if (no % 2 == 1){
					g.fillOval(x + 21, y + 21, 7, 7);
				}

				// lower-left and upper-right dots
				if (no >= 2){
					g.fillOval(x + 4, y + 39, 7, 7);
					g.fillOval(x + 39, y + 4, 7, 7);
				}

				// upper-left and lower-right dots
				if (no >= 4){
					g.fillOval(x + 4, y + 4, 7, 7);
					g.fillOval(x + 39, y + 39, 7, 7);
				}

				// left and right dots (for 6 only)
				if (no == 6){
					g.fillOval(x + 4, y + 21, 7, 7);
					g.fillOval(x + 39, y + 21, 7, 7);
				}
			}
		}

		/**
		 * Listens for ActionEvents in this AttackWindow, for JButtons
		 * Implemented method for java.awt.event.ActionListener.actionPerformed
		 */
		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {

			// user wants to exit the window
			// all attacks are final - no taking back attacks
			if (e.getSource() == okButton) {

				// if the armySlider is visible, that means the
				// user is wanting to move armies into the
				// recently conquered country
				if (armySlider.isVisible()){

					// senses if a player has died.
					for (int country = 0; country < countries.length; country++){
						if (countries[country] != defendCountry &&
								countries[country].getOwningPlayer() == defendCountry.getOwningPlayer()){
							break;
						}
						else if(country == countries.length - 1){
							JOptionPane.showMessageDialog(boardPanel, "You have destroyed " + players[defendCountry.getOwningPlayer()].getName() + " " + players[currentPlayer].getName() + "!", players[defendCountry.getOwningPlayer()].getName() + " Lost!", JOptionPane.INFORMATION_MESSAGE);
							int cardcount = 0;
							for (RiskCard card : players[defendCountry.getOwningPlayer()].getCards()){
								players[attackCountry.getOwningPlayer()].addCard(card);
								cardcount++;
							}
							JOptionPane.showMessageDialog(boardPanel, "You get " + cardcount + " RISK cards!", players[defendCountry.getOwningPlayer()].getName() + " Lost!", JOptionPane.INFORMATION_MESSAGE);
							if (cardcount > 0)
								phase = 2;
							players[defendCountry.getOwningPlayer()] = null;
						}
					}

					if (players[currentPlayer].getNoOfCards() >= 5){
						phase = 2;
						JOptionPane.showMessageDialog(boardPanel, "You got RISK PACKS!\nPlace more armies!", "MESSAGE", JOptionPane.INFORMATION_MESSAGE);
						players[currentPlayer].armiesToPlace = getArmies(true);
					}

					// checks if you have won
					for (int player = 1; player < players.length; player++){
						if (players[player] != null && players[player] != players[currentPlayer]){
							break;
						}
						else if (player == players.length - 1){
							System.out.println (players[currentPlayer].getName() + " wins the game of RISK!");
							JOptionPane.showMessageDialog(boardPanel, players[currentPlayer].getName() + " wins the game of RISK!", "GAME OVER", JOptionPane.INFORMATION_MESSAGE);
							phase = 5; // would go to default if mouse clicked
							break;
						}
					}
					// move armies, and set owning player of the
					// newly owned country
					attackCountry.removeArmies(armySlider.getValue());
					defendCountry.addArmies(armySlider.getValue());
					defendCountry.setOwningPlayer(attackCountry.getOwningPlayer());
					successfulAttack = true; 
				}
				attacking = false;
				// repaints the board panel for visual confirmation
				boardPanel.repaint();
				hide();
			}

			// perform attack based of current settings
			else if(e.getSource() == attackButton){

				// check if dice are valid (should be) and call the
				// attack() method if so
				if (Integer.parseInt("" + attackDiceSelector.getSelectedItem()) <= attackCountry.getNoOfDice() &&
						Integer.parseInt("" + defendDiceSelector.getSelectedItem()) <= defendCountry.getArmies()){
					attack();
				}

				// otherwise if you don't have enough armies to attack,
				// let them know and close the window.
				else if (attackCountry.getArmies() == 1){
					hide();
					JOptionPane.showMessageDialog(boardPanel, "You are done attacking.", "Finished Battle", JOptionPane.INFORMATION_MESSAGE);
				}

				// otherwise, they just have the wrong number of dice
				else{
					JOptionPane.showMessageDialog(this, "Invalid attack/defend dice", "Invalid Dice", JOptionPane.ERROR_MESSAGE);
				}
			}
		}

		/**
		 * Listends for any ItemEvents in this AttackWindow
		 * Implemented method for java.awt.event.ItemListener.itemStateChanged 
		 * @deprecated
		 */
		public void itemStateChanged(ItemEvent e) {
			if (e.getSource() == attackDiceSelector || e.getSource() == defendDiceSelector){
				repaint();
			}
		}

		/**
		 * Listend for and ChangeEvents in this AttackWindow
		 * Implemented method for javax.swing.event.ChangeListener.stateChanged 
		 */
		public void stateChanged(ChangeEvent e) {
			// army slider has changed - change the label to match
			// how many armies are currently selected
			if (e.getSource() == armySlider) {
				armies.setText("            Move: " + armySlider.getValue());
			}
		}

		/**
		 * Performs an attack based on the current settings,
		 * which are assumed to be error-free.
		 */
		public void attack (){

			// reinitialize attacking dice and defending dice
			attackDice = new int[Integer.parseInt("" + this.attackDiceSelector.getSelectedItem())];
			for (int die = 0; die < attackDice.length; die++){
				attackDice[die] = (int)(Math.random() * 6 + 1);
			}
			defendDice = new int[Integer.parseInt("" + this.defendDiceSelector.getSelectedItem())];
			for (int die = 0; die < defendDice.length; die++){
				defendDice[die] = (int)(Math.random() * 6 + 1);
			}

			// finds how many armies each country lost
			int firstAttack = attackDice[0];
			int secondAttack = 0;

			for (int die = 1; die < attackDice.length; die++){
				if (firstAttack < attackDice[die]){
					secondAttack = firstAttack;
					firstAttack = attackDice[die];
				}
				else if (secondAttack < attackDice[die]){
					secondAttack = attackDice[die];
				}
			}

			int firstDefend = defendDice[0];
			int secondDefend = 0;
			if (defendDice.length == 2){
				if (firstDefend < defendDice[1]){
					secondDefend = firstDefend;
					firstDefend = defendDice[1];
				}
				else{
					secondDefend = defendDice[1];
				}
			}

			//changes county armies accordingly
			if (firstAttack > firstDefend){
				defendCountry.removeArmies(1);
			}
			else{
				attackCountry.removeArmies(1);
			}

			if (secondAttack != 0 && secondDefend != 0){
				if (secondAttack > secondDefend){
					defendCountry.removeArmies(1);
				}
				else{
					attackCountry.removeArmies(1);
				}
			}

			// show the changes
			actionPanel.repaint();
		}

		/**
		 * if the window is closing,  it's the same as selecting the ok button
		 * Implemented method for java.awt.event.WindowListener.windowClosing
		 */
		public void windowClosing(WindowEvent e){
			this.actionPerformed(new ActionEvent(okButton, 0, ""));
		}

		/**
		 * Implemented method for java.awt.event.WindowListener.windowActivated
		 */
		public void windowActivated(WindowEvent e) {}

		/**
		 * Implemented method for java.awt.event.WindowListener.windowClosed
		 */
		public void windowClosed(WindowEvent e) {}

		/**
		 * Implemented method for java.awt.event.WindowListener.windowDeactivated
		 */
		public void windowDeactivated(WindowEvent e) {}

		/**
		 * Implemented method for java.awt.event.WindowListener.windowDeiconified
		 */
		public void windowDeiconified(WindowEvent e) {}

		/**
		 * Implemented method for java.awt.event.WindowListener.windowIconified
		 */
		public void windowIconified(WindowEvent e) {}

		/**
		 * Implemented method for java.awt.event.WindowListener.windowOpened
		 */
		public void windowOpened(WindowEvent arg0) {}
	}

	/**
	 * Opens a window to select options to start a new game.
	 * This will call <b>Risk</b>.newGame()
	 * @author Jamie Langille and Christopher Ng
	 * @version Jan. 2009
	 */
	private class NewGameWindow extends JFrame implements ActionListener, ItemListener
	{
		private JButton okButton, cancelButton;
		private JComboBox noOfPlayers;
		private JCheckBox randomCountries;
		private JPanel panel;
		private JTextField[] playerNames;
		private JButton[] playerColors;

		/**
		 * Constructs a NewGameWindow
		 */
		public NewGameWindow()
		{
			// window attributes
			setTitle ("New Game Setup");
			setSize(400, 400);
			setLocation(200, 200);

			// create buttons and javax.swing components
			okButton = new JButton ("OK");
			okButton.addActionListener(this);

			cancelButton = new JButton ("Cancel");
			cancelButton.addActionListener(this);

			noOfPlayers = new JComboBox ();
			for (int number = 1; number <= 6; number++){
				noOfPlayers.addItem(number);
			}
			noOfPlayers.addItemListener(this);

			randomCountries = new JCheckBox ("Random Countries", true);

			playerNames = new JTextField[7];
			for (int name = 0 ; name < playerNames.length; name++){
				playerNames[name] = new JTextField ("Player" + name, 10);
			}

			playerColors = new JButton[7];
			for (int color = 0; color < playerColors.length; color++){
				playerColors[color] = new JButton ("Select Color...");
				playerColors[color].addActionListener(this);
			}

			// construct the panel & add components
			panel = new JPanel();
			panel.setLayout(new FlowLayout(FlowLayout.CENTER, 25, 10));
			for (int player = 1; player < playerColors.length; player++){
				panel.add(playerNames[player]);
				panel.add(playerColors[player]);
			}
			panel.add(new JLabel ("Number of Players"));
			panel.add (noOfPlayers);
			panel.add(randomCountries);
			panel.add(cancelButton);
			panel.add(okButton);

			noOfPlayers.setSelectedItem(2);

			this.add(panel);

			this.setResizable(false);
			this.setAlwaysOnTop(true);
		}

		@SuppressWarnings("deprecation")
		/**
		 * Implemented meothod for java.awt.event.ActionListener.actionPerformed
		 * Listens for buttons and other ActionEvents 
		 */
		public void actionPerformed(ActionEvent e){
			if (e.getSource() == okButton){

				// (re)initialize players
				players = new Player[Integer.parseInt("" + noOfPlayers.getSelectedItem()) + 1];

				// calculate how many armies are to be placed
				int noToPlace = 0;
				switch (players.length - 1)
				{
				case 2: noToPlace = 40; break;
				case 3: noToPlace = 35; break;
				case 4: noToPlace = 30; break;
				case 5: noToPlace = 25; break;
				case 6: noToPlace = 20; break;
				}

				// create individual players passing names & colors
				for (int player = 0; player < players.length; player++){
					players[player] = new Player(playerNames[player].getText(), playerNames[player].getBackground());
					players[player].armiesToPlace = noToPlace;
				}

				// start a new game (throws IOException due to loadPlaces())
				try {
					newGame(randomCountries.isSelected());
				} catch (IOException event) {}
				hide();
			}
			else if (e.getSource() == cancelButton){
				hide();
			}
			else
			{
				// check for any of the color buttons, and
				// allow them to come on top of this window.
				this.setAlwaysOnTop(false);
				for (int player = 1; player <= Integer.parseInt("" + noOfPlayers.getSelectedItem()); player++){
					if (e.getSource() == playerColors[player]){
						Color c = JColorChooser.showDialog(null, "Choose a Color, " + playerNames[player].getText(), playerNames[player].getBackground());
						playerNames[player].setBackground(c);
					}
				}
			}
		}

		/**
		 * Implemented Method for java.awt.event.ItemListener.itemStateChanged
		 */
		public void itemStateChanged(ItemEvent e){

			// changes user names/colors and # of CPUs based on when noOfPlayers changes.
			if (e.getSource() == noOfPlayers){
				int noOfCurrentPlayers = Integer.parseInt("" + noOfPlayers.getSelectedItem());
				for (int name = 0; name < playerNames.length; name++){
					playerNames[name].setEnabled(false);
					playerColors[name].setEnabled(false);
				}
				for (int name = 1; name <= noOfCurrentPlayers; name++){
					playerNames[name].setEnabled(true);
					playerColors[name].setEnabled(true);
				}
				panel.repaint();
			}
		}
	}

	/**
	 * Inner Class for drawing the board graphics. Also used to display
	 * messages and popups, as this will always be visible to the user.
	 * @author Jamie langille and Christopher Ng
	 * @version Jan. 2009
	 */
	private class BoardPanel extends JPanel
	{
		private Country selectedCountry, attackingCountry;

		/**
		 * Constructs a new BoardPanel Object
		 * @param size		the size of this window
		 */
		public BoardPanel (Dimension size)
		{
			setPreferredSize (size);
			setBackground (new Color (0, 0, 0));

			// Add mouse listeners and Key Listeners to the drawing panel
			this.addMouseListener (new MouseHandler ());
			this.setFocusable (true); // not needed? // TODO
			this.addKeyListener (new KeyHandler ());
			this.addMouseMotionListener(new MouseMover());
			this.requestFocusInWindow (); // not needed?
		}

		/** Repaint this panel
		 * @param g 	The Graphics context
		 */
		public void paintComponent (Graphics g)
		{
			super.paintComponent (g);

			// set the font
			g.setFont(new Font("Arial", 20, 20));

			// fill in all countries and draw number of
			// armies on each
			for (Country country : countries){
				if (players != null){
					g.setColor(players[country.getOwningPlayer()].getColor());
				}
				else
					g.setColor(Color.WHITE);
				g.fillPolygon(country);
				drawArmies (country, g);
			}

			// draws a brighter country that was moused over
			if (selectedCountry != null && players != null){
				g.setColor(players[selectedCountry.getOwningPlayer()].getColor().brighter().brighter());
				g.fillPolygon(selectedCountry);
				drawArmies (selectedCountry, g);
				selectedCountry = null;
			}

			// draw all of the country outlines
			g.setColor(Color.BLACK);
			for (Country country : countries){
				g.drawPolygon(country);
			}

			// draws a selected country that will attack
			if (attackingCountry != null && players != null){
				g.setColor(players[attackingCountry.getOwningPlayer()].getColor().brighter().brighter());
				g.fillPolygon(attackingCountry);
				drawArmies (attackingCountry, g);
			}

			// draws continent connections
			drawConnection (270, 212, 296, 239, g);
			drawConnection (425, 106, 462, 139, g);
			drawConnection (558, 378, 571, 391, g);
			drawConnection (54, 261, 77, 281, g);
			drawConnection (0, 330, 44, 380, g);
			drawConnection (737, 425, 800, 485, g);

		}

		/**
		 * Sets a country as one that was moused
		 * over for painting / hilighting
		 * @param country	the country to highlight
		 */
		public void showStats(Country country){
			selectedCountry = country;
		}

		/**
		 * Sets the Country that is going to attack,
		 * so that it can be highlighted in this BoardPanel
		 * @param country
		 */
		public void attackingCountry (Country country){
			attackingCountry = country;
		}

		/**
		 * Draws how many Armies a Country has at that location
		 * @param country	the Country to draw the number of armies for
		 * @param g			the Graphics context to draw in
		 */
		public void drawArmies (Country country, Graphics g){
			g.setColor(new Color (255 - g.getColor().getRed(), 255 - g.getColor().getGreen(), 255 - g.getColor().getBlue()));
			int xValue = 0;
			int yValue = 0;
			for (int pt = 0; pt < country.npoints; pt++){
				xValue += country.xpoints[pt];
				yValue += country.ypoints[pt];
			}
			xValue /= country.npoints;
			yValue /= country.npoints;
			g.drawString("" + country.getArmies(), xValue, yValue);
		}

		/**
		 * Draws a connection between two continents
		 * @param x1	the x-value of the first point (upper-left)
		 * @param y1	the y-value of the first point (upper-left)
		 * @param x2	the x-value of the first point (lower-right)
		 * @param y2	the y-value of the first point (lower-right)
		 * @param g		the Graphics context
		 */
		public void drawConnection (int x1, int y1, int x2, int y2, Graphics g){
			g.setColor(Color.WHITE);
			g.drawOval(x1, y1, x2-x1, y2-y1);
			while (x1 < x2 && y1 < y2){
				g.drawOval(x1 += 5, y1 += 5, (x2 -= 5)-x1, (y2 -= 5)-y1);
			}
		}
	}

	/**
	 * Inner class to draw player statistics, current turn, etc.
	 * @author Jamie Langille and Christopher Ng
	 * @version Jan. 2009
	 */
	private class UserPanel extends JPanel
	{

		private Country selectedCountry;

		/**
		 * Construct a new UserPanel Object.
		 * @param size	the size of the panel
		 */
		public UserPanel (Dimension size)
		{
			setPreferredSize (size);
			setBackground (new Color (255, 255, 255));
			// Add mouse listeners and Key Listeners to the drawing panel
			this.addMouseListener (new MouseHandler ());
			this.setFocusable (true);
			this.addMouseMotionListener(new MouseMover());
			this.addKeyListener (new KeyHandler ());
			this.requestFocusInWindow ();
		}

		/** 
		 * Overriding method to javax.swing.JComponent.paintComponent
		 * Used for repainting the user panel
		 */
		public void paintComponent(Graphics g) // this is called when you use repaint()
		{
			super.paintComponent(g);
			
			// Array of x points used in the user panel
			int[]xValueLeftPoints={-300,200,300,400,500,600,700};
			
			// background
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, 800, 100);

			// draws user sections
			g.setColor (Color.BLACK);
			if (currentPlayer>0)
			{
				g.fillRect(100+(players.length*100),0,600,100);
				for (int player=1;player<players.length;player++)
				{
					if (players[player] != null){

						// draw the background of that player's color
						g.setColor (players[player].getColor());
						g.fillRect(xValueLeftPoints[player],10,100,100);

						// get the opposate color
						g.setColor(new Color (255 - g.getColor().getRed(), 255 - g.getColor().getGreen(), 255 - g.getColor().getBlue()));

						// draw player's name, armies to place, RiskCards, and what they should be doing now
						g.drawString(players[player].getName(), xValueLeftPoints[player]+2, 20);
						if (players[player].armiesToPlace < 0){
							g.drawString("Armies: 0", xValueLeftPoints[player]+2, 35);
						}
						else{
							g.drawString("Armies: " + players[player].armiesToPlace, xValueLeftPoints[player]+2, 35);
						}
						for (int card = 1; card <= players[player].getCards().length; card++){
							g.fillRect(xValueLeftPoints[player]+12 * card, 42, 10, 15);
						}
						if (players[player] == players[currentPlayer]){
							String task = "";
							switch (phase){
							case 0:
								task = "Select Country"; break;
							case 1:
								task = "Place Army"; break;
							case 2:
								task = "Place Armies"; break;
							case 3:
								task = "Attack"; break;
							case 4:
								task = "Fortify"; break;
							default:
								task = "YOU WIN";
							}
							g.drawString(task, xValueLeftPoints[player]+2, 70);
						}
					}
				}
			}

			// Draws seperations between the player sections
			g.setColor(Color.DARK_GRAY);
			g.fillRect(0,0 , 199,3 );
			g.fillRect(0,97 , 199,3 );
			g.fillRect(0,0 , 3,100 );
			g.fillRect(199,0 , 3,100 );
			
			for(int xPoint=299;xPoint<=699;xPoint+=100)
			{
				g.fillRect(xPoint,0 ,2 ,100 );
			}
			
			// top bar
			g.setColor(Color.ORANGE);
			g.fillRect(203, 0, 800, 10);

			// draws the red arrow to indicate who's turn it is currently
			g.setColor(Color.RED);
			Polygon arrow = new Polygon();
			arrow.addPoint(xValueLeftPoints[currentPlayer], 1);
			arrow.addPoint(xValueLeftPoints[currentPlayer]+99, 1);
			arrow.addPoint(xValueLeftPoints[currentPlayer]+50, 9);
			g.fillPolygon(arrow);

			// draws statistics for a country when it's become selected (mouseover)
			if (selectedCountry != null && players != null){

				// get the same selected color of that country
				g.setColor(players[selectedCountry.getOwningPlayer()].getColor().brighter().brighter());

				/* Draws name,
				 * how many armies are there,
				 * who owns it,
				 * what continent it belongs to,
				 * how many countries that continent has in total,
				 * and how much that continent is worth
				 */
				g.drawString(selectedCountry.getName(), 20, 15);
				g.drawString("Armies: " + selectedCountry.getArmies(), 20, 30);
				g.drawString("Owned by: " + players[selectedCountry.getOwningPlayer()].getName(), 20, 45);
				for (Continent continent : continents){
					if (continent.contains(selectedCountry)){
						g.drawString("In continent: " + continent.getName(), 20, 60);
						g.drawString ("No of Countries: " + continent.getNoOfCountries(), 20, 75);
						g.drawString("Bonus: " + continent.getBonus(), 20, 90);
						break;
					}
				}

				// resets the selected country
				selectedCountry = null;
			}
		}

		/**
		 * Selects a Country to show statistics in this panel
		 * @param country	the Country who's statistics will be displayed
		 */
		public void showStats(Country country) {
			selectedCountry = country;
		}
	}

	/**
	 * Inner class to handle Key Input events
	 * @author Jamie Langille and Christopher Ng
	 * @version Jan. 2009
	 */
	private class KeyHandler extends KeyAdapter
	{

		/**
		 * Overriding method to java.awt.event.KeyAdapter.keyPressed
		 * Allows a player to end attack/fortify phase by hitting 'ESC'
		 */
		public void keyPressed (KeyEvent event)
		{
			if (event.getKeyCode() == KeyEvent.VK_ESCAPE){
				// if attacking,  simply changes the phase.
				if (phase == 3){
					JOptionPane.showMessageDialog(boardPanel, "Fortify - or press ESC to end your turn.", "Fortify", JOptionPane.INFORMATION_MESSAGE);
					phase = 4;
				}

				// chose not to fortify - handle after-turn logic
				else if (phase == 4){

					// give that player a card if they attacked successfully
					if (successfulAttack){
						players[currentPlayer].addCard(deck.dealACard());
						JOptionPane.showMessageDialog(boardPanel, "You got a Risk Card " + players[currentPlayer].getName() + "!\nYou now have " + players[currentPlayer].getNoOfCards() + " card(s)!", "Risk Card", JOptionPane.INFORMATION_MESSAGE);

						// checks if the game is over
						// would set the phase to 5: see MouseHandler
						// 			would enter "default" in switch statement
						int player = countries[0].getOwningPlayer();
						for (int searchCountry = 1; searchCountry < countries.length; searchCountry++){
							if (countries[searchCountry].getOwningPlayer() == player){
								break;
							}
							else if(searchCountry == countries.length - 1){
								System.out.println (players[currentPlayer].getName() + " won the game!");// you win
								phase = 5;
								return;
							}
						}
					}

					// handle who's turn it now is
					currentPlayer++;
					if (currentPlayer == players.length)
						currentPlayer = 1;

					// skip over any eliminated players
					while (players[currentPlayer] == null){
						currentPlayer++;
						if (currentPlayer == players.length)
							currentPlayer = 1;
					}

					// changes phase & promts next player to place armies
					successfulAttack = false;
					players[currentPlayer].armiesToPlace = getArmies(false);
					phase = 2;
					JOptionPane.showMessageDialog(boardPanel, "Place your armies, " + players[currentPlayer].getName(), "Place Armies", JOptionPane.INFORMATION_MESSAGE);
				}
			}
			// repaints the board panel for visual confirmation
			boardPanel.repaint();
		}
	}
	
	/**
	 * Inner Class to handle mouse Events
	 * @version Jan. 2009
	 */
	private class MouseHandler extends MouseAdapter
	{
		private int selectedCountry = -1;
		
		/**
		 * Overriding method to java.awt.event.MouseAdapter.mousePressed
		 * Calls when the mouse is clicked
		 */
		public void mousePressed (MouseEvent event)
		{
			if (event.getComponent() == boardPanel){
				if (players != null){
					
					// finds the selected country (if there is one)
					for (Country country : countries){
						if (country.contains(event.getX(), event.getY())){
							
							// performs different actions based on the state of the game
							switch (phase){
							
							// initial selection of countries
							case 0:
							{
								
								// selects country if it's not owned by another player
								// player 0 is dummy
								if (country.getOwningPlayer() == 0){
									country.setOwningPlayer(currentPlayer);
									country.addArmies(1);
									players[currentPlayer].armiesToPlace--;

									// current player handling
									currentPlayer++;
									if (currentPlayer == players.length){
										currentPlayer = 1;
									}
								}
								else{
									JOptionPane.showMessageDialog(boardPanel, "This country is taken. Please select another one.", "Already Owned", JOptionPane.INFORMATION_MESSAGE);
								}

								// phase handling / check
								phase++;
								for (Country anotherCountry : countries){
									if (anotherCountry.getOwningPlayer() == 0){
										phase = 0;
										break;
									}
								}
								
								// promopts user to begin placing extra armies
								if (phase == 1){
									JOptionPane.showMessageDialog(boardPanel, "Place your extra armies.", "Place Armies.", JOptionPane.INFORMATION_MESSAGE);
									currentPlayer = 1;
								}
								break;
							}
							
							// initial placing of extra armies at the start of the game
							case 1:
							{
								// places an army on a country you own if you have armies left to place
								if (country.getOwningPlayer() == currentPlayer && players[currentPlayer].armiesToPlace > 0){
									country.addArmies(1);
									players[currentPlayer].armiesToPlace--;

									// current player handling
									currentPlayer++;
									if (currentPlayer == players.length){
										currentPlayer = 1;
									}
									if (phase == 2){
										JOptionPane.showMessageDialog(boardPanel, "Place your armies, " + players[currentPlayer].getName(), "Place Armies", JOptionPane.INFORMATION_MESSAGE);
									}
								}
								
								// if you don't have any left to place, prompt
								// the user that they have none left to place
								else if (players[currentPlayer].armiesToPlace == 0){
									JOptionPane.showMessageDialog(boardPanel, "You are out of armies to place.", "Out of Armies", JOptionPane.INFORMATION_MESSAGE);

									// current player handling
									currentPlayer++;
									if (currentPlayer == players.length){
										currentPlayer = 1;
									}
								}
								
								// otherwise, they chose a country that they do not own
								else{
									JOptionPane.showMessageDialog(boardPanel, "This country is owned by another player.", "Already Owned", JOptionPane.INFORMATION_MESSAGE);
								}

								// phase handling / check
								for (Player player : players){
									
									// handles even distribution of armies (everyone will have them left over)
									if (player.armiesToPlace == 0 && currentPlayer == 1){
										phase = 3;
										JOptionPane.showMessageDialog(boardPanel, "Press 'Esc' to end your attacks.", "Attack Phase", JOptionPane.INFORMATION_MESSAGE);
										break;
									}
								}
								break;
							}
							
							// placimg armies on the start of a turn
							case 2:
							{		
								// places an army on a country you own if you have armies left ot place
								if (country.getOwningPlayer() == currentPlayer && players[currentPlayer].armiesToPlace > 0){
									
									// left mouse button: 1 army
									if (event.getButton() == MouseEvent.BUTTON1){
										country.addArmies(1);
										players[currentPlayer].armiesToPlace--;
									}
									
									// right mouse button: 5 armies
									else if (event.getButton() == MouseEvent.BUTTON3 && players[currentPlayer].armiesToPlace > 4){
										country.addArmies(5);
										players[currentPlayer].armiesToPlace -= 5;
									}
									
									// center mouse button: 10 armies
									else if (event.getButton() == MouseEvent.BUTTON2 && players[currentPlayer].armiesToPlace > 9){
										country.addArmies(10);
										players[currentPlayer].armiesToPlace -= 10;
									}
									
									// if they clicked a mouse button that they cannot use (e.g. place 5 when they only have 3)
									else{
										JOptionPane.showMessageDialog(boardPanel, "You do not have enough armies.", "Out of Armies", JOptionPane.INFORMATION_MESSAGE);
									}
								}
								
								// country is already owned
								else{
									JOptionPane.showMessageDialog(boardPanel, "This country is owned by another player.", "Already Owned", JOptionPane.INFORMATION_MESSAGE);
								}

								// phase handling
								if (players[currentPlayer].armiesToPlace == 0){
									JOptionPane.showMessageDialog(boardPanel, "Press 'Esc' to end your attacks.", "Attack Phase", JOptionPane.INFORMATION_MESSAGE);
									phase++;
								}

								break;
							}
							
							// Attacks
							case 3:
							{
								// denies any user input if they are currently attacking
								// ensures only one attack at a time
								if (attacking)
								{
									JOptionPane.showMessageDialog(boardPanel, "Finish this attack first.", "You are Still Attacking", JOptionPane.INFORMATION_MESSAGE);
									break;
								}

								// if no country is selected and you pick a valid one to attack from,
								// select it.
								if (selectedCountry == -1 && country.getOwningPlayer() == currentPlayer && country.getArmies() > 1){
									for (int i = 0; i < countries.length; i++){
										if (countries[i] == country){
											selectedCountry = i;
											boardPanel.attackingCountry(countries[selectedCountry]);
											break;
										}
									}
								}
								
								// if you have a selected country to attack from and you selected a valid country
								// to attack, perform the attack.
								else if (selectedCountry != -1 &&
										countries[selectedCountry].connectsTo(country) &&
										country.getOwningPlayer() != countries[selectedCountry].getOwningPlayer()){ 

									attacking = true;
									AttackWindow window = new AttackWindow (countries[selectedCountry], country);
									window.setVisible(true);
									boardPanel.attackingCountry(null);
									selectedCountry = -1;
								}
								
								// user input fail.
								else{
									JOptionPane.showMessageDialog(boardPanel, "This doesn't work.", "This doesn't work.", JOptionPane.INFORMATION_MESSAGE);
									boardPanel.attackingCountry(null);
									attacking = false;
									selectedCountry = -1;
								}
								
								// phase would become 4 once you hit 'esc'
								
								break;
							}
							
							// fortify & end of turn
							case 4:
							{
								
								// if you haven't selected a country and this one is valid
								// select it as one to fortify from
								if (selectedCountry == -1 && country.getOwningPlayer() == currentPlayer && country.getArmies() > 1){
									for (int i = 0; i < countries.length; i++){
										if (countries[i] == country){
											selectedCountry = i;
											boardPanel.attackingCountry(countries[selectedCountry]);
											break;
										}
									}
								}
								
								// if you have a selected country and you've chosen a valid one to fortify to,
								// fortify your armies
								else if (selectedCountry != -1 &&
										countries[selectedCountry].connectsTo(country) &&
										country.getOwningPlayer() == countries[selectedCountry].getOwningPlayer()){ 

									// get user input (show min and max armies that can be moved)
									int move = 0;
									try{
										move = Integer.parseInt(JOptionPane.showInputDialog(boardPanel, "Move: ", "Move Armies: 1-" + (countries[selectedCountry].getArmies() - 1), JOptionPane.INFORMATION_MESSAGE));
									}
									catch (NumberFormatException e){ // handle invalid entries
										move = Integer.MAX_VALUE;
									}
									while(!(move < countries[selectedCountry].getArmies() && move > 0)){
										try{
											move = Integer.parseInt(JOptionPane.showInputDialog(boardPanel, "Move: ", "Move Armies: 1-" + (countries[selectedCountry].getArmies() - 1), JOptionPane.INFORMATION_MESSAGE));
										}
										catch (NumberFormatException e){
											move = Integer.MAX_VALUE;
										}
									}

									// move armies
									countries[selectedCountry].removeArmies(move);
									country.addArmies(move);

									// change selected countries, etc.
									boardPanel.attackingCountry(null);
									selectedCountry = -1;

									// if the player had a successful attack, give them a RiskCard
									if (successfulAttack){
										players[currentPlayer].addCard(deck.dealACard());
										JOptionPane.showMessageDialog(boardPanel, "You got a Risk Card " + players[currentPlayer].getName() + "!\nYou now have " + players[currentPlayer].getNoOfCards() + " card(s)!", "Risk Card", JOptionPane.INFORMATION_MESSAGE);
									}

									// current player handling
									currentPlayer++;
									if (currentPlayer == players.length)
										currentPlayer = 1;

									// hop over null players
									while (players[currentPlayer] == null){
										currentPlayer++;
										if (currentPlayer == players.length)
											currentPlayer = 1;
									}

									// change phase, and prompt next user to place thier armies
									phase = 2;									
									successfulAttack = false;
									JOptionPane.showMessageDialog(boardPanel, "Place your armies, " + players[currentPlayer].getName(), "Place Armies", JOptionPane.INFORMATION_MESSAGE);
									players[currentPlayer].armiesToPlace = getArmies(false);
								}
								
								// invalid user data
								else{
									JOptionPane.showMessageDialog(boardPanel, "This doesn't work.", "This doesn't work.", JOptionPane.INFORMATION_MESSAGE);
									boardPanel.attackingCountry(null);
									selectedCountry = -1;
								}                                                               
								break;
							}
							
							
							// would only get here if phase == 5,
							// when the game has ended.
							default:
							{
								JOptionPane.showMessageDialog(boardPanel, players[currentPlayer].getName() + " wins the game of RISK!", "GAME OVER", JOptionPane.INFORMATION_MESSAGE);
							}
							}

							// breaks out of for loop that searches through countries
							// bacause it's already found
							break;
						}
					}
				}
			}
			repaint();
		}
	}

	/**
	 * Inner class to handle mouse moved events
	 * @version Jan. 2009
	 */
	private class MouseMover extends MouseMotionAdapter
	{
		/**
		 * Overrides java.awt.event.MouseMotionAdapter.mouseMoved
		 * finds the country that was moused over in the boardPanel,
		 * and gets the boardPanel to highlight it as well as the userPanel
		 * to show the statistics for that country.
		 */
		public void mouseMoved (MouseEvent event){
			if (event.getComponent() == boardPanel){
				for (Country country : countries){
					if (country.contains(event.getX(), event.getY())){ 
						boardPanel.showStats(country);
						userPanel.showStats(country);
						break;
					}
				}
				repaint();
			}
		}
	}

	/**
	 * Inner class for <u>Player</u> Objects
	 * @version Jan. 2009
	 */
	private class Player
	{
		private Color color;
		private String name;
		public int armiesToPlace;
		private ArrayList < RiskCard > cards;

		/**
		 * Constructs a new Player Object given a name and Color
		 * @param name	the Player's name
		 * @param color	the player's color
		 */
		public Player (String name, Color color)
		{
			this.name = name;
			this.color = color;
			armiesToPlace = 0;
			cards = new ArrayList < RiskCard > (0);
		}

		/**
		 * Constructs a new Player Object. Default name is "CPU" and color is random
		 * @deprecated
		 */
		public Player ()
		{
			name = "CPU";
			color = new Color ((int) (Math.random () * 255 + 1), (int) (Math.random () * 255 + 1), (int) (Math.random () * 255 + 1));
			cards = new ArrayList < RiskCard > (0);
		}

		/**
		 * Adds a RisdkCard to the collection of RiskCards that this player has
		 * @param card
		 */
		public void addCard (RiskCard card)
		{
			cards.add (card);
		}

		/**
		 * Gets the RiskCards that this player owns as an
		 * array of RiskCards.
		 * @return		an array of RiskCards that this player owns
		 */
		public RiskCard[] getCards ()
		{
			RiskCard[] cardList = new RiskCard [cards.size ()];
			for (int card = 0 ; card < cards.size () ; card++)
			{
				cardList [card] = cards.get (card);
			}
			return cardList;
		}

		/**
		 * Returns the cards that form a Risk Pack out of the cards that this player has.
		 * If this player has a pack, this will remove the cards from this hand.
		 * @return	an array of 3 RiskCards if this player has a pack,
		 * 			null otherwise
		 */
		public RiskCard[] getPack ()
		{
			if (cards.size () >= 3)
			{
				for (int card = 0 ; card < cards.size () - 2 ; card++)
				{
					for (int secondCard = card + 1 ; secondCard < cards.size () - 1 ; secondCard++)
					{
						for (int thirdCard = secondCard + 1 ; thirdCard < cards.size () ; thirdCard++)
						{
							if ((cards.get(card).getValue() == -1 || cards.get (secondCard).getValue () == -1 || cards.get (thirdCard).getValue () == -1) || // TODO
									(cards.get(card).getValue () == cards.get (secondCard).getValue () && cards.get (secondCard).getValue () == cards.get (thirdCard).getValue ()) || (cards.get (card).getValue () != cards.get (secondCard).getValue () && cards.get (secondCard).getValue () != cards.get (thirdCard).getValue () && cards.get (thirdCard).getValue () != cards.get (card).getValue ()))
							{
								RiskCard[] pack = 
								{
										cards.get (card), cards.get (secondCard), cards.get (thirdCard)
								};
								cards.remove(pack[0]);
								cards.remove(pack[1]);
								cards.remove(pack[2]);
								return pack;
							}
						}
					}
				}
			}
			return null;
		}

		/**
		 * Retrieves all of the countries that this player owns
		 * @return	an ArrayList (java.util.ArrayList) of Country Objects that this player owns
		 */
		public ArrayList<Country> getOwningCountries()
		{
			ArrayList<Country> owningCountries= new ArrayList<Country>(0);
			for (int country =0;country<44;country++)
			{
				if (countries[country].getOwningPlayer()==currentPlayer)
					owningCountries.add (countries[country]);
			}
			return owningCountries;
		}

		/**
		 * Gets this player's color
		 * @return	this Player'c color
		 */
		public Color getColor ()
		{
			return color;
		}

		/**
		 * Changes the name of this Player
		 * @param name		the name that this Player's name will become
		 */
		public void changeName (String name)
		{
			this.name = name;
		}

		/**
		 * Gets this player's name
		 * @return	this player's name
		 */
		public String getName ()
		{
			return name;
		}

		/**
		 * Changes this Player's color
		 * @param color	the color for this Player's color to become
		 */
		public void setColor (Color color)
		{
			this.color = color;
		}

		/**
		 * Gets how many cards that this player owns
		 * @return	the number of cards that this Player owns
		 */
		public int getNoOfCards() {
			return cards.size();
		}
	}
	
	/** 
	 * Inner class for <u><b>Risk</b>Card</u> Objects
	 * @author Jamie Langille and Christopher Ng
	 * @version Jan. 2009
	 */
	private class RiskCard{
		private Country country;
		private int value;

		/**
		 * Constructs a new RiskCard Object
		 * @param country	the Country for this Card to refer to
		 * @param value		the value of this card (1, 2, 3 or -1 for wild cards)
		 */
		public RiskCard(Country country, int value){
			this.country = country;
			this.value = value;
		}

		/**
		 * Overriding method to java.lang.Object.toString
		 * Prints this card in pure text
		 */
		public String toString(){
			return "Card: " + country + ", " + value;
		}


		/**
		 * Gets the value of this card
		 * @return	the value of this card (1, 2, 3 of -1 for wild cards)
		 */
		public int getValue() {
			return this.value;
		}
		
		/**
		 * Gets the country that this RiskCard refers to
		 * @return		the Country that this country refers to
		 */
		public Country getCountry(){
			return this.country;
		}
	}

	/**
	 * Inner class for <u><b>Risk</b>Deck</u> Object
	 * @author Jamie Langille and Christopher Ng
	 * @version Jan. 2009
	 */
	private class RiskDeck
	{
		private RiskCard[] cards;
		protected int topCard = 0;

		/** Constructs a RiskDeck object
		 */
		public RiskDeck ()
		{
			cards = new RiskCard [44];
			int value = 1;
			for (int country = 0 ; country < countries.length ; country++)
			{                     
				cards [country] = new RiskCard (countries[country], value++);
				if (value == 4){
					value = 1;
				}
			}
			
			cards[42] = new RiskCard (null, -1);
			cards[43] = new RiskCard (null, -1);
			this.shuffle();
		}


		/** Shuffles this RiskDeck
		 */
		public void shuffle ()
		{
			boolean[] selected = new boolean [cards.length];
			for (int first = 0 ; first < cards.length ; first++)
			{
				int second = (int)(Math.random() * cards.length);
				while (selected[second]){
					second = (int)(Math.random() * cards.length);
				}
				selected[second] = true;
				
				RiskCard trans = cards[first];
				cards[first] = cards[second];
				cards[second] = trans;
			}
		}


		/** Deals the top card
		 * @return The card at the beginning of the deck
		 */
		public RiskCard dealACard ()
		{
			if (topCard == cards.length){
				shuffle();
				topCard = 0;
			}
			return cards [topCard++];
		}
	}

	/**
	 * Inner class for <u>Continent</u> Objects
	 * @author Jamie Langille
	 * @version Jan. 2009
	 */
	private class Continent {
		private ArrayList<Country> countries;
		private int bonus;
		private String name;

		/**
		 * Constructs a new Continent Object with a spoecified bonus value
		 * @param bonus		the bonus value of this continent
		 */
		public Continent(int bonus){
			super();
			this.bonus = bonus;
			countries = new ArrayList<Country>(0);
		}

		/**
		 * Gets the bonus that a specified player gets for owning this country
		 * @param player	the index of a Player to get the bonus of
		 * @return the bonus that a specified player gets for owning this country
		 */
		public int getBonus(int player) {
			for (Country country : countries) {
				if (country.getOwningPlayer() != player) {
					return 0;
				}
			}
			return bonus;
		}

		/**
		 * Gets the bonus value of this Continent
		 * @return		the bonus value of this Continent
		 */
		public int getBonus() {
			return bonus;
		}
		
		/**
		 * Adds a Country to this Continent
		 * @param country	the Country to add
		 */
		public void addCountry(Country country){
			countries.add(country);
		}

		/**
		 * Gets the name of this Continent Object
		 * @return		the name of this Continent
		 */
		public String getName() {
			return name;
		}

		/**
		 * Verifies wether this Continent contains a specified Country
		 * @param country	the Country to check
		 * @return			true if the Country is within this Continent
		 * 					false otherwise
		 */
		public boolean contains(Country country) {
			if (countries.contains(country))
				return true;
			return false;
		}

		/**
		 * Gets the number of Countries within this Continent
		 * @return the number of Countries within this Continent
		 */
		public int getNoOfCountries() {
			return countries.size();
		}

		/**
		 * Gets all the Countries within this Continent as an ArrayList
		 * @return	an ArrayList of all the Countries within this COntinent
		 */
		public ArrayList<Country> getCountries(){
			return countries;
		}
		
		/**
		 * Sets the name of this Continent
		 * @param name  the name that this Continent will be named
		 */
		public void setName(String name) {
			this.name = name;
		}
	}

	/**
	 * Inner class for <u>Country</u> Objects
	 * @author Jamie Langille and Christopher Ng
	 * @version Jan. 2009
	 */
	private class Country extends Polygon {
		private int owningPlayer;
		private int noOfArmies;
		protected ArrayList<Country> connections;
		private String name;

		/**
		 * Constructs a new COuntry Object given a specified name
		 * @param name		the name of this Country
		 */
		public Country(String name) {
			super();
			this.name = name;
			this.connections = new ArrayList<Country>(0);
		}

		/**
		 * Constructs a new Country Object with a blank name
		 * @deprecated
		 */
		public Country(){
			this("");
		}
		
		/**
		 * Adds a connection to another COuntry Object
		 * @param country
		 */
		public void addConnection(Country country) {
			connections.add(country);
		}

		/**
		 * Sets the owning player of this country that refers to the index of that Player
		 * @param owningPlayer		the player that will now own this Country
		 */
		public void setOwningPlayer(int owningPlayer) {
			this.owningPlayer = owningPlayer;
		}

		/**
		 * Gets the index of the player that owns this Country
		 * @return	the index of the player that owns this Country
		 */
		public int getOwningPlayer() {
			return owningPlayer;
		}
		
		/**
		 * Adds a specified ammount of Armies to this Country
		 * @param noOfArmies		the number of armies to add
		 */
		public void addArmies(int noOfArmies) {
			this.noOfArmies += noOfArmies;
		}

		/**
		 * Removes a specified ammount of armies to this Country
		 * @param noOfArmies
		 */
		public void removeArmies(int noOfArmies) {
			this.noOfArmies -= noOfArmies;
		}

		/**
		 * Gets the number of armies are in this Country
		 * @return the number of armies are in this Country
		 */
		public int getArmies() {
			return noOfArmies;
		}

		/**
		 * Determines if this Country connects to another
		 * @param country	the Country to determine if this Country connects to it
		 * @return		true if this Country Connects to it
		 * 				false otherwise
		 */
		public boolean connectsTo(Country country) {
			for (int connection = 0; connection < connections.size(); connection++) {
				if (connections.get(connection) == country)
					return true;
			}
			return false;
		}

		/**
		 * Gets the maximum number of attacking dice that this Country can use to attack
		 * @return	the maximum number of attacking dice that this Country can use to attack
		 */
		public int getNoOfDice() {
			if (noOfArmies - 1 > 3)
				return 3;
			return noOfArmies - 1;
		}

		/**
		 * Gets the name of this Country
		 * @return the name of this Country
		 */
		public String getName() {
			return name;
		}

		/**
		 * Overriding toString() method for debugging
		 * prints the country name, owning player value, and number of armies 
		 */
		public String toString() {
			return "\"" + name + "\" owned by Player" + owningPlayer + " has "
			+ noOfArmies + " armies";
		}

		/**
		 * Determines if this Country is owned by a specified player's index
		 * @param player		the index of the player
		 * @return				true if this Country is owned by the specified player
		 * 						false otherwise
		 */
		public boolean isOwnedBy(int player) {
			if (player == owningPlayer)
				return true;
			return false;
		}

		/**
		 * Gets all the countries that this Country connects to.
		 * @return	an ArrayList of all the COuntries that this Country connects to.
		 */
		public ArrayList<Country> getConnections() {
			return connections;
		}
	}
	
	/**
	 * Creates and runs the game of <b>Risk</b>
	 * @param args			Standard command-line arguments. Any arguments passed will not be used.
	 * @throws IOException	if <code><b>Risk</b> Map Points.txt</code> cannot be found
	 */
	public static void main(String[] args) throws IOException{
		Risk frame = new Risk ();
		frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible (true);
	}
}
