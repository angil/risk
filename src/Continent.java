import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.util.ArrayList;

/**
 * Constructs a Continent object
 * 
 * @author Jamie Langille
 * @version NOT COMPLETED
 */

public class Continent extends Polygon {

	private static final long serialVersionUID = 1L;
	private ArrayList<Country> countries;
	private int bonus;
	private String name;
	private Color color;

	
	public Continent(int bonus){
		super();
		this.bonus = bonus;
		countries = new ArrayList<Country>(0);
	}

	public int getBonus(int player) {
		for (Country country : countries) {
			if (country.getOwningPlayer() != player) {
				return 0;
			}
		}
		return bonus;
	}

	public int getBonus() { // should take this out (eventually)
		return bonus;
	}
	
	public void addCountry(Country country){
		countries.add(country);
	}

	public String getName() {
		return name;
	}

	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		for (Country country : countries) {
			g.drawPolygon(country);
		}
		g.setColor(color);
		g.drawPolygon(this);
	}

	public String toString() {
		return "";
	}
}
