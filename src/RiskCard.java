/** Makes a risk card object for gameplay
 * @author Jamie Langille
 * @version NOT COMPLETED
 */

import java.awt.Graphics;
import java.awt.Color;

public class RiskCard{
	private Country country;
	private int value;

	public RiskCard(Country country, int value){
		this.country = country;
		this.value = value;
	}

	public void draw (int x, int y, Graphics g){
		g.setColor (Color.BLACK); // country's color - would be determined by the player, right now != available
		g.fillPolygon (country);
		g.setColor (Color.BLACK);
		g.drawPolygon (country);
		// Watch drawing countries!! will go to default location!!
		//g.drawImage(); // for value img
	}

	public String toString(){
		return "Card: " + country + ", " + value;
	}
	
	

	public static void main (String[] args){
		RiskCard card = new RiskCard (new Country("Place"), 5);
		System.out.println (card);
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
	public Country getCountry(){
		return this.country;
	}
}
