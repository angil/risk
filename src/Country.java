import java.awt.Color;
import java.awt.Polygon;
import java.util.ArrayList;

/**
 * Used to make a country/location on a Risk board
 * 
 * @author Jamie Langille, Christopher Ng
 * @version NOT COMPLETED Dec 8 7:00 PM
 */

public class Country extends Polygon {
	private static final long serialVersionUID = 1L;
	private int owningPlayer;
	private int noOfArmies;
	private ArrayList<Country> connections;
	private String name;

	public Country(String name) {
		super();
		this.name = name;
		this.connections = new ArrayList<Country>(0);
	}
	
	public Country(){
		this("");
	}

	public void addConnection(Country country) {
		connections.add(country);
	}

	public void setOwningPlayer(int owningPlayer) {
		this.owningPlayer = owningPlayer;
	}

	public int getOwningPlayer() {
		return owningPlayer;
	}

	public void addArmies(int noOfArmies) {
		this.noOfArmies += noOfArmies;
	}

	public void removeArmies(int noOfArmies) {
		this.noOfArmies -= noOfArmies;
	}

	public int getArmies() {
		return noOfArmies;
	}

	public boolean connectsTo(Country country) {
		for (int connection = 0; connection < connections.size(); connection++) {
			if (connections.get(connection) == country)
				return true;
		}
		return false;
	}

	public int getNoOfDice() {
		if (noOfArmies >= 4)
			return 3;
		return noOfArmies - 1;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return "\"" + name + "\" owned by Player" + owningPlayer + " has "
				+ noOfArmies + " armies";
	}

	public boolean isOwnedBy(int currentPlayer) {
		if (currentPlayer == owningPlayer)
			return true;
		return false;
	}
}
