


public class RiskDeck
{
	private RiskCard[] cards = new RiskCard [52];
	protected int topCard = -1;

	/** Constructs a deck object
	 */
	public RiskDeck ()
	{
		RiskCard[] cards = new RiskCard [44];
		int currentCard = 0;
		int value;
		for (int country = 0 ; country < Risk.countries.length ; country++)
		{//array of all countries <name>                        
			value=(int)(Math.random()*3+1);
			this.cards [currentCard] = new RiskCard (Risk.countries[country], value);
			currentCard++;

		}
		cards[42]=new RiskCard (null,-1);
		cards[43]=new RiskCard (null, -1);
	}


	/** Shuffles the deck
	 */
	public void shuffle ()
	{
		for (int change = 0 ; change <= 7 * cards.length ; change++)
		{
			int firstPos = (int) (Math.random () * cards.length);
			RiskCard temp = cards [firstPos];
			int secondPos = (int) (Math.random () * cards.length);
			cards [firstPos] = cards [secondPos];
			cards [secondPos] = temp;
		}
	}


	/** Deals the top card
	 *@return The card at the beginning of the deck
	 */
	public RiskCard dealACard ()
	{
		topCard++;
		if (topCard == cards.length)
			return null;

		return cards [topCard];
	}
}


